cd /philly/eu2/pnrsy/v-yuewng/project/philly-dqn-baseline/dqn_baseline
nvidia-smi --loop=2 &
sleep 0s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Qbert -m dqn -s 0 >shelllog/Qbert_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Qbert -m dqn -s 1 >shelllog/Qbert_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Qbert -m dqn -s 2 >shelllog/Qbert_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Qbert -m dqn -s 3 >shelllog/Qbert_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Qbert -m dqn -s 4 >shelllog/Qbert_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Qbert -m dqn -s 5 >shelllog/Qbert_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Qbert -m dqn -s 6 >shelllog/Qbert_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Qbert -m dqn -s 7 >shelllog/Qbert_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Riverraid -m dqn -s 0 >shelllog/Riverraid_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Riverraid -m dqn -s 1 >shelllog/Riverraid_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Riverraid -m dqn -s 2 >shelllog/Riverraid_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Riverraid -m dqn -s 3 >shelllog/Riverraid_3.txt 2>&1  
sleep 40s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Riverraid -m dqn -s 4 >shelllog/Riverraid_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Riverraid -m dqn -s 5 >shelllog/Riverraid_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Riverraid -m dqn -s 6 >shelllog/Riverraid_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Riverraid -m dqn -s 7 >shelllog/Riverraid_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t RoadRunner -m dqn -s 0 >shelllog/RoadRunner_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t RoadRunner -m dqn -s 1 >shelllog/RoadRunner_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t RoadRunner -m dqn -s 2 >shelllog/RoadRunner_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t RoadRunner -m dqn -s 3 >shelllog/RoadRunner_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t RoadRunner -m dqn -s 4 >shelllog/RoadRunner_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t RoadRunner -m dqn -s 5 >shelllog/RoadRunner_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t RoadRunner -m dqn -s 6 >shelllog/RoadRunner_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t RoadRunner -m dqn -s 7 >shelllog/RoadRunner_7.txt 2>&1  
sleep 0s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Robotank -m dqn -s 0 >shelllog/Robotank_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Robotank -m dqn -s 1 >shelllog/Robotank_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Robotank -m dqn -s 2 >shelllog/Robotank_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Robotank -m dqn -s 3 >shelllog/Robotank_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Robotank -m dqn -s 4 >shelllog/Robotank_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Robotank -m dqn -s 5 >shelllog/Robotank_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Robotank -m dqn -s 6 >shelllog/Robotank_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Robotank -m dqn -s 7 >shelllog/Robotank_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Seaquest -m dqn -s 0 >shelllog/Seaquest_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Seaquest -m dqn -s 1 >shelllog/Seaquest_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Seaquest -m dqn -s 2 >shelllog/Seaquest_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Seaquest -m dqn -s 3 >shelllog/Seaquest_3.txt 2>&1  
sleep 40s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Seaquest -m dqn -s 4 >shelllog/Seaquest_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Seaquest -m dqn -s 5 >shelllog/Seaquest_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Seaquest -m dqn -s 6 >shelllog/Seaquest_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Seaquest -m dqn -s 7 >shelllog/Seaquest_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Skiing -m dqn -s 0 >shelllog/Skiing_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Skiing -m dqn -s 1 >shelllog/Skiing_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Skiing -m dqn -s 2 >shelllog/Skiing_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Skiing -m dqn -s 3 >shelllog/Skiing_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Skiing -m dqn -s 4 >shelllog/Skiing_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Skiing -m dqn -s 5 >shelllog/Skiing_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Skiing -m dqn -s 6 >shelllog/Skiing_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Skiing -m dqn -s 7 >shelllog/Skiing_7.txt 2>&1  