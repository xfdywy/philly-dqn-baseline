cd /philly/eu2/pnrsy/v-yuewng/project/philly-dqn-baseline/dqn_baseline
nvidia-smi --loop=2 &
sleep 0s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Frostbite -m dqn -s 0 >shelllog/Frostbite_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Frostbite -m dqn -s 1 >shelllog/Frostbite_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Frostbite -m dqn -s 2 >shelllog/Frostbite_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Frostbite -m dqn -s 3 >shelllog/Frostbite_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Frostbite -m dqn -s 4 >shelllog/Frostbite_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Frostbite -m dqn -s 5 >shelllog/Frostbite_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Frostbite -m dqn -s 6 >shelllog/Frostbite_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Frostbite -m dqn -s 7 >shelllog/Frostbite_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Gopher -m dqn -s 0 >shelllog/Gopher_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Gopher -m dqn -s 1 >shelllog/Gopher_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Gopher -m dqn -s 2 >shelllog/Gopher_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Gopher -m dqn -s 3 >shelllog/Gopher_3.txt 2>&1  
sleep 40s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Gopher -m dqn -s 4 >shelllog/Gopher_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Gopher -m dqn -s 5 >shelllog/Gopher_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Gopher -m dqn -s 6 >shelllog/Gopher_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Gopher -m dqn -s 7 >shelllog/Gopher_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Gravitar -m dqn -s 0 >shelllog/Gravitar_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Gravitar -m dqn -s 1 >shelllog/Gravitar_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Gravitar -m dqn -s 2 >shelllog/Gravitar_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Gravitar -m dqn -s 3 >shelllog/Gravitar_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Gravitar -m dqn -s 4 >shelllog/Gravitar_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Gravitar -m dqn -s 5 >shelllog/Gravitar_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Gravitar -m dqn -s 6 >shelllog/Gravitar_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Gravitar -m dqn -s 7 >shelllog/Gravitar_7.txt 2>&1  
sleep 0s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Hero -m dqn -s 0 >shelllog/Hero_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Hero -m dqn -s 1 >shelllog/Hero_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t Hero -m dqn -s 2 >shelllog/Hero_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Hero -m dqn -s 3 >shelllog/Hero_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Hero -m dqn -s 4 >shelllog/Hero_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Hero -m dqn -s 5 >shelllog/Hero_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Hero -m dqn -s 6 >shelllog/Hero_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Hero -m dqn -s 7 >shelllog/Hero_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t IceHockey -m dqn -s 0 >shelllog/IceHockey_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t IceHockey -m dqn -s 1 >shelllog/IceHockey_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t IceHockey -m dqn -s 2 >shelllog/IceHockey_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t IceHockey -m dqn -s 3 >shelllog/IceHockey_3.txt 2>&1  
sleep 40s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t IceHockey -m dqn -s 4 >shelllog/IceHockey_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t IceHockey -m dqn -s 5 >shelllog/IceHockey_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=0  nohup  python main.py -t IceHockey -m dqn -s 6 >shelllog/IceHockey_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t IceHockey -m dqn -s 7 >shelllog/IceHockey_7.txt 2>&1  &
sleep 0s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Jamesbond -m dqn -s 0 >shelllog/Jamesbond_0.txt 2>&1  &
sleep 10s && CUDA_VISIBLE_DEVICES=1  nohup  python main.py -t Jamesbond -m dqn -s 1 >shelllog/Jamesbond_1.txt 2>&1  &
sleep 20s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Jamesbond -m dqn -s 2 >shelllog/Jamesbond_2.txt 2>&1  &
sleep 30s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Jamesbond -m dqn -s 3 >shelllog/Jamesbond_3.txt 2>&1  &
sleep 40s && CUDA_VISIBLE_DEVICES=2  nohup  python main.py -t Jamesbond -m dqn -s 4 >shelllog/Jamesbond_4.txt 2>&1  &
sleep 50s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Jamesbond -m dqn -s 5 >shelllog/Jamesbond_5.txt 2>&1  &
sleep 60s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Jamesbond -m dqn -s 6 >shelllog/Jamesbond_6.txt 2>&1  &
sleep 70s && CUDA_VISIBLE_DEVICES=3  nohup  python main.py -t Jamesbond -m dqn -s 7 >shelllog/Jamesbond_7.txt 2>&1  