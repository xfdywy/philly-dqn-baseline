import os
import random
os.mkdir('dqn_baseline_bash')
os.chdir('dqn_baseline_bash')

games =  ['air_raid', 'alien', 'amidar', 'assault', 'asterix', 'asteroids', 'atlantis',
    'bank_heist', 'battle_zone', 'beam_rider', 'berzerk', 'bowling', 'boxing', 'breakout', 'carnival',
    'centipede', 'chopper_command', 'crazy_climber', 'demon_attack', 'double_dunk',
    'elevator_action', 'enduro', 'fishing_derby', 'freeway', 'frostbite', 'gopher', 'gravitar',
    'hero', 'ice_hockey', 'jamesbond', 'journey_escape', 'kangaroo', 'krull', 'kung_fu_master',
    'montezuma_revenge', 'ms_pacman', 'name_this_game', 'phoenix', 'pitfall', 'pong', 'pooyan',
    'private_eye', 'qbert', 'riverraid', 'road_runner', 'robotank', 'seaquest', 'skiing',
    'solaris', 'space_invaders', 'star_gunner', 'tennis', 'time_pilot', 'tutankham', 'up_n_down',
    'venture', 'video_pinball', 'wizard_of_wor', 'yars_revenge', 'zaxxon']

gym_games = [''.join([g.capitalize()  for g in game.split('_')])    for game in games ]


all_jobs = []
for  nii,ii in enumerate(gym_games):
    for jj in range(8):
        all_jobs.append('sleep %ds && CUDA_VISIBLE_DEVICES=abc  nohup  python main.py -t %s -m dqn -s %d >shelllog/%s_%d.txt 2>&1  '%(
                2*jj*5 , ii,  jj, ii, jj ))

# random.shuffle(all_jobs)




class bash_generator():
    def __init__(self):
        
        self.count = 0
        self.gpu_count = 0
        self.all_job_num = 0
        self.job_log = open( 'job_log.csv', 'wb')
        self.set_config()
        self.init_bash()
        self.init_fp()
        self.init_joblog()

        
        
        
    def add_one(self,job):
        self.all_job_num +=1
        self.count += 1
        self.job_log.write(', %d,%s \n'%(self.all_job_num,job))
        if (self.count )%self.game_blok == 0:
                self.bash.append(job.replace('abc' , str(self.gpu_count))+'')
        else:
                self.bash.append(job.replace('abc' , str(self.gpu_count))+'&')
        
        if self.count % 3 ==0 :
            self.gpu_count += 1
            # self.count =0
        if self.gpu_count ==4:
            self.gpu_count =0


        if self.count == self.game_per_job:

            self.write_fp()
            self.init_joblog()
            self.init_bash()
            self.init_fp()
            self.gpu_count = 0
            self.count =0
        
    
    def set_config(self,job_num=10 , game_num=480 , card_per_job =4,job_per_gpu=3):
        self.game_num=game_num
        self.job_num=job_num
        self.card_per_job=card_per_job
        self.job_per_gpu = job_per_gpu
        self.game_blok = card_per_job  *job_per_gpu 
        self.game_per_job = self.game_num / self.job_num
        # self.init_bash()
 

        
    def init_bash(self):
        self.bash=[]
        self.bash.append('cd /philly/eu2/pnrsy/v-yuewng/project/philly-dqn-baseline/dqn_baseline')
        self.bash.append('nvidia-smi --loop=2 &')

    
    def init_fp(self):
        self.fp = open('job_%d.sh'%(self.all_job_num) , 'wb') 
    
    def init_joblog(self):
        self.job_log.write('job_%d, \n'%(self.all_job_num))
    
    def write_fp(self):
        self.fp.write('\n'.join(self.bash))
        self.fp.close()
        
    def close_joblog(self):
        self.job_log.close()






a = bash_generator()
[a.add_one(x) for x in all_jobs]
a.write_fp()
a.close_joblog()
 


