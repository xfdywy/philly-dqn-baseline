import os

os.mkdir('dqn_baseline_bash')
os.chdir('dqn_baseline_bash')

games =  ['air_raid', 'alien', 'amidar', 'assault', 'asterix', 'asteroids', 'atlantis',
    'bank_heist', 'battle_zone', 'beam_rider', 'berzerk', 'bowling', 'boxing', 'breakout', 'carnival',
    'centipede', 'chopper_command', 'crazy_climber', 'demon_attack', 'double_dunk',
    'elevator_action', 'enduro', 'fishing_derby', 'freeway', 'frostbite', 'gopher', 'gravitar',
    'hero', 'ice_hockey', 'jamesbond', 'journey_escape', 'kangaroo', 'krull', 'kung_fu_master',
    'montezuma_revenge', 'ms_pacman', 'name_this_game', 'phoenix', 'pitfall', 'pong', 'pooyan',
    'private_eye', 'qbert', 'riverraid', 'road_runner', 'robotank', 'seaquest', 'skiing',
    'solaris', 'space_invaders', 'star_gunner', 'tennis', 'time_pilot', 'tutankham', 'up_n_down',
    'venture', 'video_pinball', 'wizard_of_wor', 'yars_revenge', 'zaxxon']

gym_games = [''.join([g.capitalize()  for g in game.split('_')])    for game in games ]

for  nii,ii in enumerate(gym_games):
        for jj in range(5):

                with open('%d_%s_%d.sh'%(nii,ii,jj) , 'w') as fp:
                        bash =[]
                        bash.append('cd /philly/wu2/pnrsy/v-yuewng/project/philly-dqn-baseline/dqn_baseline')
                        bash.append('python main.py -t %s -m dqn -s %d &'%(ii, 2*jj))
                        bash.append('sleep 1s && python main.py -t %s -m dqn -s %d'%(ii, 2*jj+1))

                        fp.write('\n'.join(bash))
