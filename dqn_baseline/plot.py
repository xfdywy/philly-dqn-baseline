import  numpy as np
import matplotlib.pyplot as plt
import os
import argparse
import re
import scipy
import pandas as pd
import numpy as np
import cPickle

parser = argparse.ArgumentParser(description='plot')
parser.add_argument('-d' , '--logdir' ,type=str , default='log_iue_new')
parser.add_argument('-f','--logfile',type=str,default='Pong')
parser.add_argument('-m', '--mode' , type = str , default= 'train')
parser.add_argument('-n' , '--new_scan' ,  type =str , default='t')
parser.add_argument('-w' , '--windows' , type = int,default=None)
parser.add_argument('-s' , '--save' , type =str ,default='f')

args = parser.parse_args()

logfile = args.logfile

all_logs = os.listdir(args.logdir)
logfilepaths = [os.path.join(args.logdir , x) for x in all_logs if logfile in x and '.txt' in x]
pklfilepaths = [os.path.join(args.logdir , x)  for x in all_logs if logfile in x and '.pkl' in x]

print(logfilepaths)
data_all = {}
testdata_all = {}
testepis_all = {}

p = re.compile(r'.* episode (\d*),.*reward(.*),.*avg reward(.*),.*total steps(.*),.*episode step(.*)')
testp = re.compile(r'.*Avg reward (.*)\(.*\)')

def process_filename(x):
    return(os.path.splitext(x)[0])


if pklfilepaths is not None and args.new_scan=='f':
    print('loading data from pkl file')
    for pklfilepath in pklfilepaths:
        with open(pklfilepath,'rb') as f:
            data,testdata,testepis = cPickle.load(f)
            name = process_filename(pklfilepath)
            data_all[name] = data
            testdata_all[name] = testdata
            testepis_all[name] = testepis
else:
    print('scan log file...')
    for logfilepath in logfilepaths:
        data = [[], [], [], [], []]
        start = False
        testdata = []
        testepis = []
        point = 0
        name = process_filename(logfilepath)
        with open(logfilepath) as f:
            for line in f:
                # print(line)
                a = p.search(line)
                # print(1)
                if a:
                    info = [a.group(x) for x in range(1, 6)]
                    for nii, ii in enumerate(info):
                        data[nii].append(float(ii))
                # print(2)
                if 'Testing' in line:
                    start = True
                if start:
                    testinfo = testp.search(line)
                    if testinfo:
                        testdata.append(float(testinfo.group(1)))
                        testepis.append(data[-2][-1])
                        start = False
            data_all[name] = data
            testdata_all[name] = testdata
            testepis_all[name] = testepis
            if args.save == 't':
                cPickle.dump([data,testdata,testepis],open(name+'.pkl','wb'))
print('load data done')

# logfilepath = './log/'+logfile + 'NoFrameskip-v4.txt'
#test = '[INFO    ] (MainProcess) episode 39, reward 5.000000, avg reward 2.461538, total steps 3498, episode step 128'
#test1 = '[INFO    ] (MainProcess) Avg reward 0.600000(0.154919)'

#test.match(test1)



        #print(3)



def plot_with_avg(x,y,window):
    x = pd.DataFrame(x)
    y = pd.DataFrame(y)

    y = y.rolling(window).mean()
    plt.plot(x,y)

if args.windows is None and args.mode == 'train':
    args.windows = 100
elif args.windows is None and args.mode =='test':
    args.windows = 5

if args.mode == 'test':
    legendlist =[]
    for ii in testdata_all.keys():

        plot_with_avg(testepis_all[ii] , testdata_all[ii],args.windows)
        legendlist.append(ii)
    # plt.plot(testepis , testdata)
    plt.title('test')
    plt.legend(legendlist)
    plt.show()
elif args.mode == 'train':
    legendlist =[]
    for ii in data_all.keys():

        plot_with_avg(data_all[ii][3] , data_all[ii][1] , args.windows)
        legendlist.append(ii)
        # plt.hold('True')
    # plt.plot(testepis , testdata)
    plt.title('train '+logfile)
    plt.legend(legendlist)
    plt.show()

    # data = pd.DataFrame(data[1])
    # data = data.rolling(100).mean()
    # plt.plot(data)
    # plt.title(logfile)
    # plt.show()


