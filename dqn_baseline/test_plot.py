import matplotlib.pyplot as plt
import argparse

import os
import re
import pandas as pd
parser = argparse.ArgumentParser(description='plot test')
parser.add_argument('-d' , '--test_res_dir' ,type=str , default='test_result')
parser.add_argument('-f','--logfile',type=str,default='Pong')
# parser.add_argument('-m', '--mode' , type = str , default= 'train')
# parser.add_argument('-n' , '--new_scan' ,  type =str , default='t')
parser.add_argument('-w' , '--windows' , type = int,default=1)
# parser.add_argument('-s' , '--save' , type =str ,default='f')
args = parser.parse_args()

alltxt = os.listdir(os.path.join(args.test_res_dir  , 'text'))

p = re.compile(args.logfile)

alltxt= [x for x in alltxt if p.search(x)]
if alltxt:

    name = os.path.splitext(alltxt[0])[0]
    loginfo = name.split('_')
    now_ind = 0
    env_name = loginfo[0]

    if loginfo[2] == 'iue':
        mode = 'dqn_iue'
        now_ind = 3
    else :
        mode = 'dqn'
        now_ind = 2

    moment_hidden_num = int(loginfo[now_ind])
    now_ind +=1
    moment_hidden = loginfo[now_ind : now_ind+moment_hidden_num]
    now_ind += moment_hidden_num
    var = float(loginfo[now_ind])
    now_ind+=1
    eb = float(loginfo[now_ind])
    now_ind +=1
    mxb = float(loginfo[now_ind])
    now_ind +=1
    suff = loginfo[now_ind]
    assert now_ind == len(loginfo)-2




else:
    assert 1==2

img_file_path =os.path.join('test_result/img' ,env_name  + '.png')
legend =[]
for this_txt in alltxt:
    name = os.path.splitext(this_txt)
    this = os.path.join('test_result/text' , this_txt)
    legend.append(name[0].split('_')[1:])
    # img_file_path =os.path.join('test_result/img' , name[0] + '.png')

    this_file = open(this,'r')
    res = [[] , [] ,[]]
    all_content = this_file.readlines()
    re_pre = re.compile(r'time step:(.*), mean reward:(.*), std reward:(.*)\n')
    for ii in all_content:
        search_result = re_pre.search(ii)
        res[0].append(int(search_result.group(1)))
        res[1].append(search_result.group(2))
        res[2].append(search_result.group(3))

    # res.sort(key=lambda x:x[0])
    # print(res[2])
    mean = pd.DataFrame(res[1])
    mean = mean.rolling(args.windows).mean()
    # plt.figure()
    plt.plot(res[0] ,mean  )
    this_file.close()

    # plt.show()
plt.legend(legend)
plt.title(env_name)
plt.savefig(img_file_path , dpi=300)
plt.show()
plt.close()

