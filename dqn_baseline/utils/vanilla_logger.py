import numpy as np
import logging
import os
import sys
'''
class Logger(object):
    def __init__(self, log_dir, vanilla_logger, skip=False):
        """Create a summary writer logging to log_dir."""
        self.info = vanilla_logger.info
        self.debug = vanilla_logger.debug
        self.warning = vanilla_logger.warning
        self.skip = skip
        logging.info('')

    def scalar_summary(self, tag, value, step):
        if self.skip:
            return

    def image_summary(self, tag, images, step):
        if self.skip:
            return

    def histo_summary(self, tag, values, step, bins=1000):
        if self.skip:
            return
'''
def Logger(log_file, verbose=1):
   # if not os.path.exists(log_file):
   #     os.mkdir(log_file)
   logger      = logging.getLogger()
   ch          = logging.StreamHandler(sys.stdout)
   formatter   = logging.Formatter('[%(asctime)s] (%(processName)-11s) %(message)s')
   fileHandler = logging.FileHandler(log_file, 'w')
   ch          = logging.StreamHandler(sys.stdout)
   fileHandler.setFormatter(formatter)
   ch.setFormatter(formatter)
   logger.addHandler(fileHandler)
   logger.addHandler(ch)
   # logger.setLevel(logging.INFO)
   # if verbose >= 2:
   #     logger.setLevel(logging.DEBUG)
   # elif verbose >= 1:
   #     logger.setLevel(logging.INFO)
   # else:
   #     # NOTE: we currently use this level to log to get rid of visdom's info printouts
   #     logger.setLevel(logging.WARNING)

   logger.setLevel(logging.INFO)
   # logger.warning  = printlogger(logger.warning)

   return logger
